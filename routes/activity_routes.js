var express = require('express');
var router = express.Router();
var activity_dal = require('../model/activity_dal');

// View All addresses
router.get('/all', function(req, res) {
    activity_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('activity/activityAll', { 'result': result });
        }
    });

});

module.exports = router;
