var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
    user_dal.countOfActivities(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('index', {
                title: 'Active',
                'aerobic': result[0][0],
                'calisthenics': result[1][0],
                'general': result[2][0],
                'sports': result[3][0],
                'weights': result[4][0]
            });
        }
    });
});



module.exports = router;
