var express = require('express');
var router = express.Router();
var goals_dal = require('../model/goals_dal');
var activity_dal = require('../model/activity_dal');
var user_dal = require('../model/user_dal');


// View All goals
router.get('/all', function(req, res) {
    goals_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('goals/goalsAll', { 'result': result });
        }
    });
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    activity_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('goals/goalsAdd', { 'Activities': result});
        }
    });
});

router.get('/insert', function(req, res) {
    if (req.query.GoalName == "") {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('goals/goalsAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else if (req.query.UserID == null) {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('goals/goalsAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else if (req.query.ActivityID == null) {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('goals/goalsAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else {
        goals_dal.insert(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                user_dal.getById(req.query.UserID, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('user/userViewById', {
                            'User': result[0][0],
                            'Address': result[1][0],
                            'Goals': result[2],
                            'Activities': result[3],
                            'UserID': req.query.UserID,
                            goal_was_successful: true
                        });
                    }
                });            }
        });
    }
});


module.exports = router;
