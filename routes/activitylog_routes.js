var express = require('express');
var router = express.Router();
var activitylog_dal = require('../model/activitylog_dal');
var activity_dal = require('../model/activity_dal');
var user_dal = require('../model/user_dal');


// View All activities logged
router.get('/all', function(req, res) {
    activitylog_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('activitylog/activitylogAll', { 'result': result });
        }
    });

});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    activity_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('activitylog/activitylogAdd', { 'Activities': result});
        }
    });
});

router.get('/insert', function(req, res) {
    if (req.query.UserID == null) {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('activitylog/activitylogAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else if (req.query.ActivityID == null) {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('activitylog/activitylogAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else if (req.query.DateDone == "") {
        activity_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('activitylog/activitylogAdd', { 'Activities': result, was_unsuccessful: true});
            }
        });
    }
    else {
        activitylog_dal.insert(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                user_dal.getById(req.query.UserID, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('user/userViewById', {
                            'User': result[0][0],
                            'Address': result[1][0],
                            'Goals': result[2],
                            'Activities': result[3],
                            'UserID': req.query.UserID,
                            log_was_successful: true
                        });
                    }
                });            }
        });
    }
});

module.exports = router;
