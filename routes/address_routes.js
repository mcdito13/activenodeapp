var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal');

// View All addresses
router.get('/all', function(req, res) {
    address_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('address/addressAll', { 'result': result });
        }
    });

});

// Return the add a new address form
router.get('/add', function(req, res){
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('address/addressAdd', { 'result': result });
        }
    });
});

// View the address for the given ID
router.get('/', function(req, res){
    if(req.query.AddressID == null) {
        res.send('AddressID is null');
    }
    else {
        address_dal.getById(req.query.AddressID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('address/addressViewById', { 'result': result });
            }
        });
    }
});

module.exports = router;
