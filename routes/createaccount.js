var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('createaccount',  { was_unsuccessful: false });
});

module.exports = router;
