var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');

// View All goals
router.get('/all', function(req, res) {
    user_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('user/usersAll', { 'result': result });
        }
    });
});

router.get('/view', function(req, res){
    if(req.query.UserID == "") {
        res.render('login', { was_unsuccessful: true });
    }
    else {
        user_dal.getById(req.query.UserID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('user/userViewById', {
                    'User' : result[0][0],
                    'Address' : result[1][0],
                    'Goals' : result[2],
                    'Activities' : result[3] });
            }
        });
    }
});

 router.get('/insert', function(req, res) {
     if (req.query.FirstName == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.LastName == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.Email == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.Height == null) {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.Weight == null) {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.StreetNo == null) {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.Street == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.City == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.State == "") {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else if (req.query.Zip == null) {
         res.render('createAccount', { was_unsuccessful: true });
     }
     else {
         user_dal.insertAddress(req.query, function (err, result) {
             var AddressID = result.insertId;
             user_dal.insertUser(req.query, AddressID, function (err, result) {
                 var UserID = result.insertId;
                 if (err) {
                     res.send(err);
                 }
                 else {
                     user_dal.getById(UserID, function (err, result) {
                         if (err) {
                             res.send(err);
                         }
                         else {
                             res.render('user/userViewById', {
                                 'User': result[0][0],
                                 'Address': result[1][0],
                                 'Goals': result[2],
                                 'Activities': result[3],
                                 'UserID': UserID,
                                 was_successful: true
                             });
                         }
                     });
                 }
                 ;
             });
         });
     }
     ;
 });

module.exports = router;
