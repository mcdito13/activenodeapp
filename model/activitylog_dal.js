var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM P1_ActivityLog;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO P1_ActivityLog (UserID, ActivityID, Location, Distance, Duration, DateDone, Weight, Sets, Reps) VALUES (?);';
    var queryData = [params.UserID, params.ActivityID, params.Location, params.Distance, params.Duration, params.DateDone, params.Weight, params.Sets, params.Reps];
    console.log(query);

    connection.query(query, [queryData], function (err, result) {
        callback(err, result);
    });
};