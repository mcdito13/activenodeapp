var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM P1_Address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(AddressID, callback) {
    var query = 'SELECT * FROM P1_Address WHERE AddressID = ?';
    var queryData = [AddressID];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO P1_Address (StreetNo, Street, City, State, Zip) VALUES (?)';
    var queryData = [params.StreetNo, params.Street, params.City, params.State, params.Zip];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(AddressID, callback) {
    var query = 'DELETE FROM P1_Address WHERE AddressID = ?';
    var queryData = [AddressID];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(params, callback) {
    var query = 'UPDATE P1_Address SET StreetNo = ?, Street = ?, City = ?, Zip = ? WHERE AddressID = ?';
    var queryData = [params.StreetNo, params.Street, params.City, params.State, params.Zip];
    console.log(query);

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};