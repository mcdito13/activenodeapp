var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM P1_User;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(UserID, callback) {
    var query = 'CALL getUserProfile(?)';
    var queryData = [UserID];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insertAddress = function(params, callback) {
    var query = 'INSERT INTO P1_Address (StreetNo, Street, City, State, Zip) VALUES (?)';
    var queryData = [params.StreetNo, params.Street, params.City, params.State, params.Zip];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insertUser = function(params, AddressID, callback) {
    var query = 'INSERT INTO P1_User (FirstName, LastName, Email, Height, Weight, AddressID) VALUES (?)';
    var queryData = [params.FirstName, params.LastName, params.Email, params.Height, params.Weight, AddressID];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.countOfActivities = function(callback) {
    var query = 'CALL activityTypeCounts';
    console.log(query);

    connection.query(query, function (err, result) {
        callback(err, result);
    });
};