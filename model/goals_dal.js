var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM P1_Goals;';
    console.log(query);

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO P1_Goals (UserID, GoalName, ActivityID, Duration, Distance, Weight, Reps) VALUES (?)';
    var queryData = [params.UserID, params.GoalName, params.ActivityID, params.Duration, params.Distance, params.Weight, params.Reps];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};